from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

from univtop.apps.main.models import *

########################################
#                                      #
#      DEFINE CLASS ADMIN AREA         #
#                                      #
########################################
class SocialFriendListAdmin(admin.ModelAdmin):
    list_display = ['user_social_auth',"friends"]


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user',"avatar"]


class EmailTrackingAdmin(admin.ModelAdmin):
    list_display = ['pk','to_emails',"email_template","text_content","context_data","status","send_time"]



########################################
#                                      #
#            REGISTER AREA             #
#                                      #
########################################
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(SocialFriendList,SocialFriendListAdmin)
admin.site.register(EmailTracking,EmailTrackingAdmin)



