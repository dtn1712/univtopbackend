import os, sys, string, datetime

SETTING_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_PATH = os.path.abspath(__file__ + "/../../../")

sys.path.append(SETTING_PATH)
sys.path.append(PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from univtop.apps.main.models import *

from django import db

def main():
	print "... RUNNING CLEANING DUMMY DATA SCRIPT ..."
	try:
		
		print "Clean Dummy Data Successfully"
	except:
		print "Clean Dummy Data Failed"
		raise

	db.close_connection()

if __name__ == '__main__':
	stage = sys.argv[1]
	if len(sys.argv) == 3 and stage == "dev" and sys.argv[2] == "--cleandummydata":
		main()